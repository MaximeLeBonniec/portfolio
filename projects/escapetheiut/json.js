var lock = true;
var nMessage = 0;
var currentRoom;
var visited = [];
var condName = [];
var condVal = [];
var done = [];
var inventory = [];
var bout = [];

function delDialP()
{
    var area = currentRoom.json.area;

    $(".dial p").on("click", function(){
        $(this).remove();
        nMessage--;
        if (nMessage == 0)
            lock = false;
    });
}

function mouseHover(areas)
{
    $(".game .areas .area").mouseenter(function(){
        if (!lock)
        {
            var vue = $(".vue").text();
            var ind = $(this).index();
            $(".dial").append("<p>(" + areas[vue - 1][ind].action.text + ")</p>");
        }
    }).mouseleave(function(){
        if (!lock)
        {
            $(".dial p").remove();
        }
    }).mouseup(function(){
        if (!lock)
        {
            $(".dial p").remove();

			var vue = $(".vue").text();
            var ind = $(this).index();
            var action = areas[vue - 1][ind].action;
			
			if ($(".room").text() == "3" && vue == 32)
			{
				done.push("horloge");
			}
			
			if ("onClick" in action)
			{
				lock = true;
				if ("ifDone" in action.onClick)
				{
					if (done.indexOf(action.onClick.ifDone) != -1)
					{
						if ("message" in action.onClick);
							$(".dial").append("<p>" + action.onClick.message + "</p>");
						nMessage = 1;
						done.push(action.onClick.done);
						delDialP();
					}
				}
			}
			
			if ("condition" in action)
			{
				if ("onClick" in action.condition)
				{
					if ("done" in action.condition.onClick)
					{
						done.push(action.condition.onClick.done);
					}
				}
				
				var room = $(".roomName").text();
				if ("visited" in action.condition)
				{
					var indCond = condName.indexOf(room + '-' + vue);
					if (condVal[indCond] != action.condition.required)
					{
						$(".dial").append("<p>" + action.condition.textIfFalse + "</p>");
						return;
					}
				}
				else if ("done" in action.condition)
				{
					var indCond = done.indexOf(action.condition.done);
					if (indCond == -1)
					{
						$(".dial").append("<p>" + action.condition.textIfFalse + "</p>");
						return;
					}
					else if ("ifDone" in action.condition)
					{
						if ("done" in action.condition.ifDone)
							done.push(action.condition.ifDone.done);
						else if ("addInventory" in action.condition.ifDone)
							addInventory(action.condition.ifDone.addInventory);
					}
				}
				else if ("obtained" in action.condition)
				{
					var indCond = inventory.indexOf(action.condition.obtained);
					if (indCond == -1)
					{
						$(".dial").append("<p>" + action.condition.textIfFalse + "</p>");
						return;
					}
				}
			}
			
            if (action.name == "changeView")
            {
                var nextVue = action.nextView;
                $(".vue").text(nextVue);
                $(".game .area").remove();
                $(".game").attr("style", "background-image: url('./ressources/Views/" + currentRoom.json.images[nextVue - 1] + "')");
                loadVue();
            }
			else if (action.name == "openUI")
			{
				$(".game .area").remove();
				loadUI(action.UI);
			}
			else if (action.name == "addInventory")
			{
				addInventory(action.add);
			}
			else if (action.name == "changeRoom")
			{
				$(".vue").text(action.nextView);
				var vue = $(".vue").text();
				$(".room").text(action.nextRoom);
				currentRoom = new Room(action.nextRoom);
                $(".game .area").remove();
                $(".game").attr("style", "background-image: url('./ressources/Views/" + currentRoom.json.images[vue - 1] + "')");
				$(".roomName").text(currentRoom.json.name);
				loadVue();
			}
        }
    });
}

function areaMouse(areas)
{
    var i = 0;
    var vue = $(".vue").text();
    areas[vue - 1].forEach(function(area)
    {
        var w = area.points.p2[0] - area.points.p1[0];
        var h = area.points.p2[1] - area.points.p1[1];
        var t = area.points.p1[1];
        var l = area.points.p1[0];
        $(".game .areas").append('<div class="area" style="width: ' + w + '%; height: ' + h + '%; top: ' + t + '%; left: ' + l + '%;" val="' + i + '"></div>');
        i++;
    });

    mouseHover(areas);
}

function initCondition(areas, vue, room)
{
	areas[vue - 1].forEach(function(area){
		if ("condition" in area.action) {
			condName.push(room + '-' + vue);
			condVal.push(area.action.condition.default);
		}
	});
}

function updateCond(areas, vue, room)
{
	var indCond = condName.indexOf(room + '-' + vue);
	
	areas[vue - 1].forEach(function(area){
		if ("condition" in area.action)
		{
			if ("visited" in area.action.condition)
			{
				if (visited.indexOf(room + '-' + area.action.condition.visited) != -1)
				{
					condVal[indCond] = area.action.condition.required;
				}
			}
			else if ("done" in area.action.condition)
			{
				if (done.indexOf(area.action.condition.done) != -1)
				{
					condVal[indCond] = area.action.condition.required;
				}
			}
			else if ("obtained" in area.action.condition)
			{
				
				if (inventory.indexOf(area.action.condition.obtained) != -1)
				{
					condVal[indCond] = area.action.condition.required;
				}
			}
		}
	});
}

function loadVue()
{
	$(".areas *").remove();
	var vue = $(".vue").text();
	var room = $(".roomName").text();
	var area = currentRoom.json.area;
	if (visited.indexOf(room + '-' + vue) == -1)
	{
		visited.push(room + '-' + vue);
		//console.table(visited);
		initCondition(area, vue, room);
	}
    areaMouse(area);
	updateCond(area, vue, room);
}

function loadUI(n)
{
	if (n == 5) {
		$(".tel").attr("val", 1);
		done.push("UI5");
	}
	$(".UI").eq(n - 1).attr("style", "display: flex !important;");
}

function UIHandler()
{
	$(".UI .close").on("click", function(){
		$(this).parent().attr("style", "");
		$(".error").remove();
		loadVue();
	});
	
	// Salle 1
	
	$(".computer #pc_submit").on("click", function(){
		var vue = $(".vue").text();
		var area = currentRoom.json.area;
		var ind = 0;
		var login = area[vue - 1][ind].action.user;
		var pass = area[vue - 1][ind].action.pass;
		if ($("#pc_name").val() == login && $("#pc_pass").val() == pass)
		{
			done.push("UI" + $(this).parent().attr("ui"));
			$(".error").remove();
			$(".computer .container").append("<p>Identifiants corrects</p>");
			$(".computer .container").attr("style", "display: none;");
			$(".computer .postit").attr("style", "display: flex;");
		}
		else
		{
			$(".error").remove();
			$(".computer .container").append("<p class='error' stye='color: red;'>Identifiants incorrects</p>");
		}
	});
	
	$("#chest_submit").on("click", function(){
		var vue = $(".vue").text();
		var area = currentRoom.json.area;
		var ind = 1;
		
		var un = $("#chest_un").val() == area[vue - 1][ind].action.digits[0];
		var deux = $("#chest_deux").val() == area[vue - 1][ind].action.digits[1];
		var trois = $("#chest_trois").val() == area[vue - 1][ind].action.digits[2];
		var quatre = $("#chest_quatre").val() == area[vue - 1][ind].action.digits[3];
		
		if (un && deux && trois && quatre)
		{
			done.push("UI" + $(this).parent().attr("ui"));
			$(".error").remove();
			$(this).parent().append("<p>Vous avez récupéré la clef</p>");
			addInventory("key1");
			addInventory("bout1");
		}
		else
		{
			$(".error").remove();
			$(this).parent().append("<p class='error' stye='color: red;'>Code incorrect</p>");
		}
	});
	
	$("#digicode_reset").on("click", function(){
		$(".actual").text(0);
		$("#digi_un, #digi_deux, #digi_trois, #digi_quatre").text("X");
		$(".error").remove();
	});
	
	$(".btn").on("click", function(){
		var actual = parseInt($(".actual").text()) + 1;
		if (actual == 5)
			actual = 1;
		
		var val = parseInt($(this).text());
		console.log(val);
		
		if (actual == 1)
		{
			$("#digi_un").text(val);
		}
		else if (actual == 2)
		{
			$("#digi_deux").text(val);
		}
		else if (actual == 3)
		{
			$("#digi_trois").text(val);
		}
		else if (actual == 4)
		{
			$("#digi_quatre").text(val);
		}
		
		$(".actual").text(actual);
	});
	
	$("#digicode_submit").on("click", function(){
		var un = $("#digi_un").text();
		var deux = $("#digi_deux").text();
		var trois = $("#digi_trois").text();
		var quatre = $("#digi_quatre").text();
		
		if (un == 'X' || deux == 'X' || trois == 'X' || quatre == 'X')
		{
			$(".error").remove();
			$(this).parent().append("<p class='error' stye='color: red;'>Code incomplet</p>");
			return;
		}
		
		un = parseInt(un);
		deux = parseInt(deux);
		trois = parseInt(trois);
		quatre = parseInt(quatre);
		
		if (un == 1 && deux == 1 && trois == 0 && quatre == 1)
		{
			$(this).parent().append("<p>Code correct</p>");
			done.push("UI" + $(this).parent().attr("ui"));
		}
		else
		{
			$(".error").remove();
			$(this).parent().append("<p class='error' stye='color: red;'>Code incorrect</p>");
			return;
		}
	});
	
	$("#switch_reset").on("click", function(){
		$(".cabledeux").attr("style", "");
		$(".switch .line").attr("etat", 0);
		$(".error").remove();
	});
	
	$(".switch .cabledeux").on("click", function(){
		$(".error").remove();
		var etat = $(this).parent().attr("etat");
		if (etat == 0)
		{
			$(this).parent().attr("etat", 1);
			$(this).attr("style", "background:" + $(this).parent().attr("val") + " !important;");
		}
		else
		{
			$(this).parent().attr("etat", 0);
			$(this).attr("style", "");
		}
	});
	
	$("#switch_submit").on("click", function(){
		var red = $(".red").parent().attr("etat");
		var green = $(".green").parent().attr("etat");
		var yellow = $(".yellow").parent().attr("etat");
		var blue = $(".blue").parent().attr("etat");
		var black = $(".black").parent().attr("etat");
		var white = $(".white").parent().attr("etat");
		
		if (red == 1 && green == 1 && yellow == 0 && blue == 1 && black == 0 && white == 1)
		{
			$(".error").remove();
			$(this).parent().append("<p>La connexion a été rétablie</p>");
			done.push("UI" + $(this).parent().attr("ui"));
			addInventory("bout5");
		}
		else
		{
			$(".error").remove();
			$(this).parent().append("<p class='error'>Mauvaise combinaison</p>");
		}
	});
}

function addInventory(toAdd)
{	
	if (inventory.indexOf(toAdd) == -1)
	{
		var img;
		if (toAdd == "key1")
			img = "clef";
		else if (toAdd == "bout1")
		{
			img = "morceauClef/bout1";
			bout.push(1);
		}
		else if (toAdd == "bout2")
		{
			img = "morceauClef/bout2";
			bout.push(2);
		}
		else if (toAdd == "bout3")
		{
			img = "morceauClef/bout3";
			bout.push(3);
		}
		else if (toAdd == "bout4")
		{
			bout.push(4);
			img = "morceauClef/bout4";
		}
		else if (toAdd == "bout5")
		{
			bout.push(5);
			img = "morceauClef/bout5";
		}
		else if (toAdd == "phone")
			img = "tel";
		
		$(".inventory").append("<div class='item " + img + "' style='background-image:url(./ressources/textures/" + img + ".png);'></div>");
	
		if (toAdd == "phone")
		{
			$(".tel").attr("val", "0");
			$(".tel").mouseleave(function(){
				$(".dial p").remove();
			}).mouseup(function(){
				var val = $(".tel").attr("val");
				if (val == 0)
					$(".dial").append("<p>Pas de réseau...<p>");
				else
					loadUI(5);
			});
		}
		
		inventory.push(toAdd);
	}
	else {
		lock = true;
		$(".dial").append("<p>Vous avez déjà ramassé cet objet</p>");
		nMessage = 1;
		delDialP();
	}
	
	if (bout.indexOf(1) != -1 && bout.indexOf(2) != -1 && bout.indexOf(3) != -1 && bout.indexOf(4) != -1 && bout.indexOf(5) != -1)
	{
		$(".inventory div").remove();
		done.push("game");
		lock = true;
		$(".dial").append("<p>VOUS POUVEZ DESORMAIS QUITTER L'IUT</p>");
		nMessage = 1;
		delDialP();
	}
}

$(function(){
	UIHandler();
	
    var r1 = new Room(1);

    currentRoom = r1;
    $(".vue").text(1);
    $(".roomName").text(currentRoom.json.name);

    loadVue();

    if ("onStart" in currentRoom.json) {
        var getDialog = $.getJSON("dialogs/" + currentRoom.json["onStart"] + ".json", function () {
            console.log("dialog import")
        }).done(function () {
            lock = true;
            var dialog = getDialog.responseJSON;

            dialog["dialogs"].forEach(function (text) {
                $(".dial").append("<p>" + text + "</p>");
                nMessage++;
            });

            delDialP();
        });
    }
});