var rooms = [
  {
    "name": "Salle de TD",
    "id": 1,
    "area": [
      [
        {
          "points": {
            "p1": [
              28.61,
              5.49
            ],
            "p2": [
              70.03,
              59.76
            ]
          },
          "action": {
            "condition": {
              "default": false,
              "required": true,
              "visited": 3,
              "textIfFalse": "Oops... Je ne me souviens plus des identifiants de connection..."
            },
            "name": "openUI",
            "UI": 1,
            "user": "jb80",
            "pass": "enmille",
            "text": "Aller sur le PC"
          }
        },
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 2,
            "text": "Se lever"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              50,
              2.59
            ],
            "p2": [
              54.55,
              12.02
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 3,
            "text": "Aller au tableau"
          }
        },
        {
          "points": {
            "p1": [
              8.67,
              41.39
            ],
            "p2": [
              39.48,
              81.42
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 1,
            "text": "Regarder le PC"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              1.54,
              46.04
            ],
            "p2": [
              5.47,
              55.46
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 4,
            "text": "Regarder le bureau"
          }
        },
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 2,
            "text": "Regarder devant le PC"
          }
        },
        {
          "points": {
            "p1": [
              95.08,
              46.04
            ],
            "p2": [
              99.14,
              55.46
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 5,
            "text": "Regarder la porte"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              50.06,
              87.29
            ],
            "p2": [
              54.49,
              96.86
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 3,
            "text": "Retourner devant le tableau"
          }
        },
        {
          "points": {
            "p1": [
              37.82,
              21.99
            ],
            "p2": [
              51.04,
              41.8
            ]
          },
          "action": {
            "condition": {
              "default": false,
              "required": true,
              "done": "UI1",
              "textIfFalse": "Il me faudrait plus d'informations concernant le code..."
            },
            "name": "openUI",
            "UI": 2,
            "text": "Regarder le coffre",
            "digits": [
              1,
              2,
              3,
              4
            ]
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 3,
            "text": "Retourner devant le tableau"
          }
        },
        {
          "points": {
            "p1": [
              38.87,
              3.42
            ],
            "p2": [
              57.44,
              84.15
            ]
          },
          "action": {
            "condition": {
              "default": false,
              "required": true,
              "obtained": "key1",
              "textIfFalse": "Il semblerait qu'il faille une clef pour pouvoir ouvrir la porte..."
            },
            "name": "changeRoom",
            "nextView": 1,
            "nextRoom": 2,
            "text": "Sortir"
          }
        }
      ]
    ],
    "images": [
      "salleTp/1.png",
      "salleTp/2.png",
      "salleTp/3.png",
      "salleTp/4.png",
      "salleTp/5.png",
      "salleTp/6.png"
    ],
    "onStart": "begining"
  },
  {
    "name": "Couloir Etage",
    "id": 2,
    "area": [
      [
        {
          "points": {
            "p1": [
              1.54,
              46.04
            ],
            "p2": [
              5.47,
              55.46
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 5,
            "text": "Aller vers la gauche"
          }
        },
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeRoom",
			"nextRoom": 1,
            "nextView": 5,
            "text": "Aller dans la salle de TP"
          }
        },
        {
          "points": {
            "p1": [
              50,
              4.37
            ],
            "p2": [
              54.49,
              11.48
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 2,
            "text": "Aller en face"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              1.54,
              46.04
            ],
            "p2": [
              5.47,
              55.46
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 27,
            "text": "Voir l'affiche à gauche"
          }
        },
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 1,
            "text": "Aller en arrière"
          }
        },
        {
          "points": {
            "p1": [
              95.08,
              46.04
            ],
            "p2": [
              99.14,
              55.46
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 21,
            "text": "Voir l'affiche à droite"
          }
        },
        {
          "points": {
            "p1": [
              50,
              4.37
            ],
            "p2": [
              54.49,
              11.48
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 3,
            "text": "Avancer"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 7,
            "text": "Faire demi tour"
          }
        },
        {
          "points": {
            "p1": [
              95.08,
              46.04
            ],
            "p2": [
              99.14,
              55.46
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 24,
            "text": "Regarder l'affiche à droite"
          }
        },
        {
          "points": {
            "p1": [
              50,
              4.37
            ],
            "p2": [
              54.49,
              11.48
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 4,
            "text": "Avancer"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 8,
            "text": "Faire demi-tour"
          }
        },
        {
          "points": {
            "p1": [
              12.3,
              0
            ],
            "p2": [
              40.47,
              41.66
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 31,
            "text": "Regarder l'emploi du temps"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 1,
            "text": "Retourner devant la porte de TP"
          }
        },
        {
          "points": {
            "p1": [
              50,
              4.37
            ],
            "p2": [
              54.49,
              11.48
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 6,
            "text": "Avancer"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 15,
            "text": "Faire demi-tour"
          }
        },
        {
          "points": {
            "p1": [
              50,
              4.37
            ],
            "p2": [
              54.49,
              11.48
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 20,
            "text": "Aller devant la salle du chef"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              14.88,
              6.01
            ],
            "p2": [
              23.43,
              80.19
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 32,
            "text": "Aller dans la salle réseau"
          }
        },
        {
          "points": {
            "p1": [
              1.54,
              46.04
            ],
            "p2": [
              5.47,
              55.46
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 21,
            "text": "Regarder l'affiche à gauche"
          }
        },
        {
          "points": {
            "p1": [
              95.08,
              46.04
            ],
            "p2": [
              99.14,
              55.46
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 27,
            "text": "Regarder l'affiche à droite"
          }
        },
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 3,
            "text": "Faire demi-tour"
          }
        },
        {
          "points": {
            "p1": [
              50,
              4.37
            ],
            "p2": [
              54.49,
              11.48
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 10,
            "text": "Avancer"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              1.54,
              46.04
            ],
            "p2": [
              5.47,
              55.46
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 24,
            "text": "Regarder l'affiche à gauche"
          }
        },
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 4,
            "text": "Faire demi-tour"
          }
        },
        {
          "points": {
            "p1": [
              50,
              4.37
            ],
            "p2": [
              54.49,
              11.48
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 7,
            "text": "Avancer"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 4,
            "text": "Faire demi-tour"
          }
        },
        {
          "points": {
            "p1": [
              50,
              4.37
            ],
            "p2": [
              54.49,
              11.48
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 8,
            "text": "Avancer"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 2,
            "text": "Faire demi-tour"
          }
        },
        {
          "points": {
            "p1": [
              50,
              4.37
            ],
            "p2": [
              54.49,
              11.48
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 1,
            "text": "Aller devant la salle de TP"
          }
        },
        {
          "points": {
            "p1": [
              1.54,
              46.04
            ],
            "p2": [
              5.47,
              55.46
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 32,
            "text": "Aller devant la salle réseau"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              50,
              4.37
            ],
            "p2": [
              54.49,
              11.48
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 12,
            "text": "Monter les escaliers"
          }
        },
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 11,
            "text": "Rez-de-chaussée"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              50,
              4.37
            ],
            "p2": [
              54.49,
              11.48
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 13,
            "text": "Aller à l'étage"
          }
        },
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 16,
            "text": "Descendre"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 17,
            "text": "Descendre"
          }
        },
        {
          "points": {
            "p1": [
              50,
              4.37
            ],
            "p2": [
              54.49,
              11.48
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 20,
            "text": "Aller dans le couloir"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 20,
            "text": "Aller devant la salle du chef"
          }
        },
        {
          "points": {
            "p1": [
              50,
              4.37
            ],
            "p2": [
              54.49,
              11.48
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 15,
            "text": "Aller dans le couloir"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 6,
            "text": "Faire demi-tour"
          }
        },
        {
          "points": {
            "p1": [
              50,
              4.37
            ],
            "p2": [
              54.49,
              11.48
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 1,
            "text": "Aller devant la salle de TP"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 18,
            "text": "Se retourner vers Philibert",
			"onClick":
			{
				"ifDone": "entrance",
				"message": "AFFRONTEZ DESORMAIS LES DIEUX DE L'IUT",
				"done": "philibert1"
			}
          }
        },
        {
          "points": {
            "p1": [
              50,
              4.37
            ],
            "p2": [
              54.49,
              11.48
            ]
          },
          "action": {
            "name": "changeRoom",
			"nextRoom": 3,
            "nextView": 1,
            "text": "Aller au rez-de-chaussée"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 13,
            "text": "Faire demi-tour"
          }
        },
        {
          "points": {
            "p1": [
              50,
              4.37
            ],
            "p2": [
              54.49,
              11.48
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 18,
            "text": "Voir Philibert"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              1.54,
              46.04
            ],
            "p2": [
              5.47,
              55.46
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 12,
            "text": "Monter"
          }
        },
        {
          "points": {
            "p1": [
              95.08,
              46.04
            ],
            "p2": [
              99.14,
              55.46
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 16,
            "text": "Descendre"
          }
        },
        {
          "points": {
            "p1": [
              37.88,
              7.38
            ],
            "p2": [
              65.93,
              90.03
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 18,
            "text": "Regarder Philibert"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              1.54,
              46.04
            ],
            "p2": [
              5.47,
              55.46
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 20,
            "text": "Aller dans le couloir"
          }
        },
        {
          "points": {
            "p1": [
              50,
              4.37
            ],
            "p2": [
              54.49,
              11.48
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 17,
            "text": "Descendre"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              95.08,
              46.04
            ],
            "p2": [
              99.14,
              55.46
            ]
          },
          "action": {
            "condition": {
              "default": false,
              "required": true,
              "done": "UI3",
              "textIfFalse": "Les portes de sécurité sont actives, je ne peux pas passer"
            },
            "name": "changeView",
            "nextView": 19,
            "text": "Passer les portes de sécurité"
          }
        },
        {
          "points": {
            "p1": [
              1.54,
              46.04
            ],
            "p2": [
              5.47,
              55.46
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 14,
            "text": "Aller dans le couloir"
          }
        },
        {
          "points": {
            "p1": [
              33.09,
              39.75
            ],
            "p2": [
              37.52,
              54.78
            ]
          },
          "action": {
            "name": "openUI",
            "UI": 4,
            "text": "Regarder le digicode"
          }
        },
        {
          "points": {
            "p1": [
              40.59,
              7.51
            ],
            "p2": [
              65.93,
              100
            ]
          },
          "action": {
            "condition": {
              "default": false,
              "required": true,
              "done": "UI4",
              "textIfFalse": "La porte est fermée, il faudrait indiquer un code sur le digicode..."
            },
            "name": "changeView",
            "nextView": 30,
            "text": "Entrer dans la salle"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              46.37,
              4.78
            ],
            "p2": [
              58.43,
              44.4
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 22,
            "text": "Voir de plus près"
          }
        },
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 7,
            "text": "Partir explorer"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 21,
            "text": "Reculer"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 21,
            "text": "Reculer"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              40.6,
              6.15
            ],
            "p2": [
              57.01,
              56.97
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 25,
            "text": "Voir de plus près"
          }
        },
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 3,
            "text": "Partir explorer"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 24,
            "text": "Reculer"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 24,
            "text": "Reculer"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              43.05,
              23.22
            ],
            "p2": [
              58.86,
              72.4
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 28,
            "text": "Voir de plus près"
          }
        },
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 2,
            "text": "Partir explorer"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 27,
            "text": "Reculer"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 27,
            "text": "Reculer"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              37.43,
              60
            ],
            "p2": [
              41.02,
              68
            ]
          },
          "action": {
            "name": "addInventory",
            "add": "bout2",
            "text": "Piece de puzzle"
          }
        },
        {
          "points": {
            "p1": [
              71.86,
              62
            ],
            "p2": [
              76.95,
              68
            ]
          },
          "action": {
            "name": "addInventory",
            "add": "phone",
            "text": "Prendre le téléphone"
          }
        },
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 20,
            "text": "Sortir"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              1.54,
              46.04
            ],
            "p2": [
              5.47,
              55.46
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 9,
            "text": "Aller dans le couloir"
          }
        },
        {
          "points": {
            "p1": [
              30.01,
              7.92
            ],
            "p2": [
              46.06,
              29.37
            ]
          },
          "action": {
            "name": "openUI",
            "UI": 6,
            "text": "Voir l'emploi du temps"
          }
        },
        {
          "points": {
            "p1": [
              71.96,
              53.28
            ],
            "p2": [
              75.77,
              69.67
            ]
          },
          "action": {
            "condition": {
              "default": false,
              "required": true,
              "obtained": "phone",
              "textIfFalse": "Il me faudrait un téléphone pour voir le QR code"
            },
            "name": "openUI",
            "UI": 5,
            "text": "Voir le QR code"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 7,
            "text": "Sortir de la salle réseau"
          }
        },
        {
          "points": {
            "p1": [
              52.46,
              30.6
            ],
            "p2": [
              65.37,
              39.89
            ]
          },
          "action": {
            "condition": {
              "default": false,
              "required": true,
              "done": "UI5",
              "textIfFalse": "Brancher des cables au hasard serait une mauvaise chose..."
            },
            "name": "openUI",
            "UI": 3,
            "text": "Regarder le switch"
          }
        }
      ]
    ],
    "images": [
      "EtageEscalier/1.png",
      "EtageEscalier/2.png",
      "EtageEscalier/3.png",
      "EtageEscalier/4.png",
      "EtageEscalier/5.png",
      "EtageEscalier/6.png",
      "EtageEscalier/7.png",
      "EtageEscalier/8.png",
      "EtageEscalier/9.png",
      "EtageEscalier/10.png",
      "EtageEscalier/11.png",
      "EtageEscalier/12.png",
      "EtageEscalier/13.png",
      "EtageEscalier/14.png",
      "EtageEscalier/15.png",
      "EtageEscalier/16.png",
      "EtageEscalier/17.png",
      "EtageEscalier/18.png",
      "EtageEscalier/19.png",
      "EtageEscalier/20.png",
      "EtageEscalier/21.png",
      "EtageEscalier/22.png",
      "EtageEscalier/23.png",
      "EtageEscalier/24.png",
      "EtageEscalier/25.png",
      "EtageEscalier/26.png",
      "EtageEscalier/27.png",
      "EtageEscalier/28.png",
      "EtageEscalier/29.png",
      "EtageEscalier/30.png",
      "EtageEscalier/31.png",
      "EtageEscalier/32.png"
    ],
    "onStart": "begining2"
  },
  {
    "name": "Bas",
    "id": 3,
    "area": [
      [
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 28,
            "text": "Se retourner"
          }
        },
        {
          "points": {
            "p1": [
              95.08,
              46.04
            ],
            "p2": [
              99.14,
              55.46
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 34,
            "text": "Regarder l'affiche"
          }
        },
        {
          "points": {
            "p1": [
              50,
              4.37
            ],
            "p2": [
              54.49,
              11.48
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 2,
            "text": "Aller en face"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              1.54,
              46.04
            ],
            "p2": [
              5.47,
              55.46
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 15,
            "text": "Aller à gauche"
          }
        },
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 1,
            "text": "Reculer"
          }
        },
        {
          "points": {
            "p1": [
              95.08,
              46.04
            ],
            "p2": [
              99.14,
              55.46
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 36,
            "text": "Regarder l'affiche"
          }
        },
        {
          "points": {
            "p1": [
              50,
              4.37
            ],
            "p2": [
              54.49,
              11.48
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 4,
            "text": "Avancer"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 26,
            "text": "Reculer"
          }
        },
        {
          "points": {
            "p1": [
              95.08,
              46.04
            ],
            "p2": [
              99.14,
              55.46
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 15,
            "text": "Aller à droite"
          }
        },
        {
          "points": {
            "p1": [
              50,
              4.37
            ],
            "p2": [
              54.49,
              11.48
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 27,
            "text": "Avancer"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 2,
            "text": "Reculer"
          }
        },
        {
          "points": {
            "p1": [
              50,
              4.37
            ],
            "p2": [
              54.49,
              11.48
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 5,
            "text": "Avancer"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              1.54,
              46.04
            ],
            "p2": [
              5.47,
              55.46
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 6,
            "text": "Aller à gauche"
          }
        },
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 4,
            "text": "Reculer"
          }
        },
        {
          "points": {
            "p1": [
              95.08,
              46.04
            ],
            "p2": [
              99.14,
              55.46
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 40,
            "text": "Regarder l'affiche"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 5,
            "text": "Reculer"
          }
        },
        {
          "points": {
            "p1": [
              50,
              4.37
            ],
            "p2": [
              54.49,
              11.48
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 7,
            "text": "Avancer"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 6,
            "text": "Reculer"
          }
        },
        {
          "points": {
            "p1": [
              50,
              4.37
            ],
            "p2": [
              54.49,
              11.48
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 8,
            "text": "Avancer"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 7,
            "text": "Reculer"
          }
        },
        {
          "points": {
            "p1": [
              50,
              4.37
            ],
            "p2": [
              54.49,
              11.48
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 9,
            "text": "Avancer"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 8,
            "text": "Reculer"
          }
        },
        {
          "points": {
            "p1": [
              50,
              4.37
            ],
            "p2": [
              54.49,
              11.48
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 10,
            "text": "Avancer"
          }
        },
        {
          "points": {
            "p1": [
              14.82,
              0
            ],
            "p2": [
              37.64,
              100
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 20,
            "text": "Voir la machine à café"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 9,
            "text": "Reculer"
          }
        },
        {
          "points": {
            "p1": [
              50,
              4.37
            ],
            "p2": [
              54.49,
              11.48
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 11,
            "text": "Avancer"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 29,
            "text": "Aller à l'entrée"
          }
        },
        {
          "points": {
            "p1": [
              50,
              4.37
            ],
            "p2": [
              54.49,
              11.48
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 19,
            "text": "Aller vers la machine à café"
          }
        },
        {
          "points": {
            "p1": [
              95.08,
              46.04
            ],
            "p2": [
              99.14,
              55.46
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 12,
            "text": "Aller dans le couloir"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              50,
              4.37
            ],
            "p2": [
              54.49,
              11.48
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 13,
            "text": "Avancer"
          }
        },
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 11,
            "text": "Reculer"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              50,
              4.37
            ],
            "p2": [
              54.49,
              11.48
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 14,
            "text": "Avancer"
          }
        },
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 12,
            "text": "Reculer"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              50,
              4.37
            ],
            "p2": [
              54.49,
              11.48
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 3,
            "text": "Avancer"
          }
        },
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 13,
            "text": "Reculer"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 2,
            "text": "Reculer"
          }
        },
        {
          "points": {
            "p1": [
              50,
              4.37
            ],
            "p2": [
              54.49,
              11.48
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 16,
            "text": "Avancer"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 15,
            "text": "Reculer"
          }
        },
        {
          "points": {
            "p1": [
              50,
              4.37
            ],
            "p2": [
              54.49,
              11.48
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 17,
            "text": "Avancer"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 16,
            "text": "Reculer"
          }
        },
        {
          "points": {
            "p1": [
              50,
              4.37
            ],
            "p2": [
              54.49,
              11.48
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 18,
            "text": "Avancer"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 17,
            "text": "Reculer"
          }
        },
        {
          "points": {
            "p1": [
              50,
              4.37
            ],
            "p2": [
              54.49,
              11.48
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 11,
            "text": "Avancer"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              1.54,
              46.04
            ],
            "p2": [
              5.47,
              55.46
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 22,
            "text": "Aller dans le couloir"
          }
        },
        {
          "points": {
            "p1": [
              95.08,
              46.04
            ],
            "p2": [
              99.14,
              55.46
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 11,
            "text": "Aller vers le hall"
          }
        },
        {
          "points": {
            "p1": [
              22.2,
              6.56
            ],
            "p2": [
              32.16,
              93.31
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 20,
            "text": "Regarder la machine à café"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 9,
            "text": "Reculer"
          }
        },
        {
          "points": {
            "p1": [
              56.03,
              86.48
            ],
            "p2": [
              58.3,
              92.35
            ]
          },
          "action": {
            "name": "addInventory",
            "add": "bout3",
            "text": "Regarder d'encore plus près"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 20,
            "text": "Reculer"
          }
        },
        {
          "points": {
            "p1": [
              56.03,
              86.48
            ],
            "p2": [
              58.3,
              92.35
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 21,
            "text": "Prendre la pièce de puzzle"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              50,
              4.37
            ],
            "p2": [
              54.49,
              11.48
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 23,
            "text": "Avancer"
          }
        },
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 9,
            "text": "Reculer"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              50,
              4.37
            ],
            "p2": [
              54.49,
              11.48
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 24,
            "text": "Avancer"
          }
        },
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 22,
            "text": "Reculer"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              50,
              4.37
            ],
            "p2": [
              54.49,
              11.48
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 25,
            "text": "Avancer"
          }
        },
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 23,
            "text": "Reculer"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              95.08,
              46.04
            ],
            "p2": [
              99.14,
              55.46
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 26,
            "text": "Aller à droite"
          }
        },
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 24,
            "text": "Reculer"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              1.54,
              46.04
            ],
            "p2": [
              5.47,
              55.46
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 40,
            "text": "Regarder l'affiche"
          }
        },
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 25,
            "text": "Reculer"
          }
        },
        {
          "points": {
            "p1": [
              50,
              4.37
            ],
            "p2": [
              54.49,
              11.48
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 27,
            "text": "Avancer"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              1.54,
              46.04
            ],
            "p2": [
              5.47,
              55.46
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 38,
            "text": "Regarder l'affiche"
          }
        },
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 3,
            "text": "Reculer"
          }
        },
        {
          "points": {
            "p1": [
              50,
              4.37
            ],
            "p2": [
              54.49,
              11.48
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 28,
            "text": "Avancer"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              1.54,
              46.04
            ],
            "p2": [
              5.47,
              55.46
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 34,
            "text": "Regarder l'affiche"
          }
        },
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 27,
            "text": "Reculer"
          }
        },
        {
          "points": {
            "p1": [
              50,
              4.37
            ],
            "p2": [
              54.49,
              11.48
            ]
          },
          "action": {
            "name": "changeRoom",
			"nextRoom": 2,
            "nextView": 16,
            "text": "Aller dans l'escalier"
          }
        },
        {
          "points": {
            "p1": [
              1.17,
              15.57
            ],
            "p2": [
              6.64,
              40.71
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 34,
            "text": "Regarder l'affiche"
          }
        },
        {
          "points": {
            "p1": [
              41.14,
              30.05
            ],
            "p2": [
              48.28,
              65.3
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 31,
            "text": "Aller dans la salle de TD"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              95.08,
              46.04
            ],
            "p2": [
              99.14,
              55.46
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 11,
            "text": "Aller dans le couloir"
          }
        },
        {
          "points": {
            "p1": [
              33.09,
              12.43
            ],
            "p2": [
              59.66,
              88.93
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 30,
            "text": "Se rapprocher de la sortie"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 29,
            "text": "Reculer"
          }
        },
        {
          "points": {
            "p1": [
              34.44,
              0
            ],
            "p2": [
              92.07,
              85.66
            ]
          },
          "action": {
            "condition": {
              "default": false,
              "required": true,
              "done": "game",
              "textIfFalse": "Philibert !?",
			  "onClick":
			  {
				  "done": "entrance"
			  }
            },
            "name": "changeView",
            "nextView": 41,
            "text": "Sortir et s'échapper"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              41.63,
              32.79
            ],
            "p2": [
              52.33,
              58.88
            ]
          },
          "action": {
			"condition":
			{
				"done": "horloge",
				"default": false,
                "required": true,
                "textIfFalse": "Quelque chose semble déconnecté du temps",
				"ifDone": {
					"addInventory": "bout4"
				}
			},
            "name": "changeView",
            "nextView": 31,
            "text": "Regarder dans l'armoire"
          }
        },
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 28,
            "text": "Sortir de la salle de TD"
          }
        },
        {
          "points": {
            "p1": [
              72.57,
              14.75
            ],
            "p2": [
              75.89,
              20.36
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 33,
            "text": "Regarder l'horloge"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 31,
            "text": "Reculer"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 31,
            "text": "Reculer"
          }
        },
        {
          "points": {
            "p1": [
              51.41,
              20.77
            ],
            "p2": [
              54.18,
              25.68
            ]
          },
          "action": {
            "condition": {
              "default": false,
              "required": true,
              "done": "bielwaski",
              "textIfFalse": "A quoi ça pourrait bien me servir ?"
            },
            "name": "changeView",
            "nextView": 32,
            "text": "Changer l'heure"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 28,
            "text": "Reculer"
          }
        },
        {
          "points": {
            "p1": [
              42.93,
              6.83
            ],
            "p2": [
              58.73,
              55.19
            ]
          },
          "action": {
            "condition": {
              "default": false,
              "required": true,
              "done": "philibert1",
              "textIfFalse": "Je ne vois rien de particulier",
			  "ifDone": {
                "done": "bielwaski"
				}
            },
            "name": "changeView",
            "nextView": 35,
            "text": "Regarder de plus près"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 28,
            "text": "Reculer"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 27,
            "text": "Reculer"
          }
        },
        {
          "points": {
            "p1": [
              33.76,
              4.51
            ],
            "p2": [
              53.07,
              62.84
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 37,
            "text": "Regarder de plus près"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 27,
            "text": "Reculer"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 27,
            "text": "Reculer"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 26,
            "text": "Reculer"
          }
        }
      ],
      [
        {
          "points": {
            "p1": [
              50,
              88.52
            ],
            "p2": [
              54.49,
              96.45
            ]
          },
          "action": {
            "name": "changeView",
            "nextView": 26,
            "text": "Reculer"
          }
        }
      ]
    ],
    "images": [
      "Bas/1.png",
      "Bas/2.png",
      "Bas/3.png",
      "Bas/4.png",
      "Bas/5.png",
      "Bas/6.png",
      "Bas/7.png",
      "Bas/8.png",
      "Bas/9.png",
      "Bas/10.png",
      "Bas/11.png",
      "Bas/12.png",
      "Bas/13.png",
      "Bas/14.png",
      "Bas/15.png",
      "Bas/16.png",
      "Bas/17.png",
      "Bas/18.png",
      "Bas/19.png",
      "Bas/20.png",
      "Bas/21.png",
      "Bas/22.png",
      "Bas/23.png",
      "Bas/24.png",
      "Bas/25.png",
      "Bas/26.png",
      "Bas/27.png",
      "Bas/28.png",
      "Bas/29.png",
      "Bas/30.png",
      "Bas/31.png",
      "Bas/32.png",
      "Bas/33.png",
      "Bas/34.png",
      "Bas/35.png",
      "Bas/36.png",
      "Bas/37.png",
      "Bas/38.png",
      "Bas/39.png",
      "Bas/40.png",
      "Bas/41.png"
    ],
    "onStart": "begining2"
  }
];